/* SPDX-License-Identifier: BSD-2-Clause */
/*
 * Copyright (c) 2023, Alibaba Group Holding Limited
 */

#ifndef TA_FCE_H
#define TA_FCE_H

/*
 * This UUID is generated with uuidgen
 * the ITU-T UUID generator at http://www.itu.int/ITU-T/asn1/uuid.html
 */
#define TA_FCE_UUID	\
        { 0xc0043c15, 0x959b, 0x4e97, \
                { 0xbe, 0x33, 0xa3, 0xc0, 0xd0, 0xf5, 0x88, 0x72} }

/* The function IDs implemented in this TA */


#define	TA_FCE_QUERY_STATE					0
#define	TA_FCE_QUERY_ISR_CNT				1
#define	TA_FCE_QUERY_RESULT_CNT				2
#define	TA_FCE_SET_RESULT_CNT				3
#define	TA_FCE_SET_ATTR						4
#define	TA_FCE_GET_ATTR						5
#define	TA_FCE_SET_CHN_ID					6
#define	TA_FCE_START						7
#define	TA_FCE_STOP							8
#define	TA_FCE_GET_RESULT					9
#define	TA_FCE_SUSPEND						10
#define	TA_FCE_RESUME						11
#define	TA_FEATURE_LIB_CREATE				12
#define	TA_FEATURE_LIB_FREE					13
#define	TA_FEATURE_LIB_SET					14

#endif /* TA_FCE_H */
