// SPDX-License-Identifier: BSD-2-Clause
/*
 * Copyright (c) 2014, STMicroelectronics International N.V.
 * Copyright (c) 2020, Linaro Limited
 */

#include <fce_hal.h>
#include <fce_ta.h>
#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>
#include <util.h>

/*
 * Called when the instance of the TA is created. This is the first call in
 * the TA.
 */
TEE_Result TA_CreateEntryPoint(void)
{
	DMSG("has been called");

	return TEE_SUCCESS;
}

/*
 * Called when the instance of the TA is destroyed if the TA has not
 * crashed or panicked. This is the last call in the TA.
 */
void TA_DestroyEntryPoint(void)
{
	DMSG("has been called");
}

/*
 * Called when a new session is opened to the TA. *sess_ctx can be updated
 * with a value to be able to identify this session in subsequent calls to the
 * TA. In this function you will normally do the global initialization for the
 * TA.
 */
TEE_Result TA_OpenSessionEntryPoint(uint32_t param_types,
		TEE_Param __maybe_unused params[4],
		void __maybe_unused **sess_ctx)
{
	TEE_Result res;
	csi_fce_handle_t fce_handle;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	res = csi_fce_open(&fce_handle, "fce_demo");
	/* Unused parameters */
	(void)&params;
	*sess_ctx = (void *)fce_handle;

	/* If return value != TEE_SUCCESS the session will not be created. */
	return res;
}

/*
 * Called when a session is closed, sess_ctx hold the value that was
 * assigned by TA_OpenSessionEntryPoint().
 */
void TA_CloseSessionEntryPoint(void __maybe_unused *sess_ctx)
{
	
	csi_fce_handle_t fce_handle ;

	fce_handle = (csi_fce_handle_t)sess_ctx;
	csi_fce_close(fce_handle);
}

static TEE_Result fce_query_result_cnt(void *sess_ctx, uint32_t param_types,
										TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_OUTPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	return csi_fce_query_result_cnt(fce_handle, &params[0].value.a);
}

static TEE_Result fce_set_result_cnt(void *sess_ctx, uint32_t param_types,
									TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_INPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	return csi_fce_set_result_cnt(fce_handle, &params[0].value.a);
}

static TEE_Result fce_set_attr(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;
	
	return csi_fce_set_attr(fce_handle, (struct fce_cfg *)params[0].memref.buffer);
}

static TEE_Result fce_get_attr(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_OUTPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;
	
	return csi_fce_get_attr(fce_handle, (struct fce_cfg *)params[1].memref.buffer);
}

static TEE_Result fce_do_compare(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;
	
	return csi_fce_do_compare(fce_handle, 
							(struct target_vector *)params[0].memref.buffer);
}


static TEE_Result fce_stop(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");
	(void) params;

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;
	
	return csi_fce_stop(fce_handle);
}

static TEE_Result fce_get_result(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_OUTPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;
	
	return csi_fce_get_result(fce_handle, (struct top_result *)params[0].memref.buffer);
}

static TEE_Result fce_create_featurelib(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	csi_fce_libbuf_handle_t handle;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_INPUT,
						   TEE_PARAM_TYPE_VALUE_OUTPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;
	
	handle = csi_fce_create_featurelib(fce_handle, params[0].value.a);

	reg_pair_from_64((unsigned long)handle, &params[1].value.a, &params[1].value.b);

	return TEE_SUCCESS;
}

static TEE_Result fce_release_featurelib(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	csi_fce_libbuf_handle_t handle;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_INPUT,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	handle = (csi_fce_libbuf_handle_t)reg_pair_to_64(params[0].value.a, params[0].value.b);

	csi_fce_release_featurelib(fce_handle, handle);

	return TEE_SUCCESS;
}

static TEE_Result fce_set_featurelib(void *sess_ctx, uint32_t param_types,
								TEE_Param params[4])
{
	csi_fce_handle_t fce_handle = (csi_fce_handle_t)sess_ctx;
	struct fce_lib_buf_info info;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_INPUT,
						   TEE_PARAM_TYPE_VALUE_INPUT,
						   TEE_PARAM_TYPE_MEMREF_INPUT,
						   TEE_PARAM_TYPE_NONE);

	DMSG("has been called");

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	info.buf_handle = (void *)reg_pair_to_64(params[0].value.a, params[0].value.b);
	info.offset = reg_pair_to_64(params[1].value.a, params[1].value.b);
	info.src = params[2].memref.buffer;
	info.size = params[2].memref.size;

	return csi_fce_set_featurelib(fce_handle, &info);
}
/*
 * Called when a TA is invoked. sess_ctx hold that value that was
 * assigned by TA_OpenSessionEntryPoint(). The rest of the paramters
 * comes from normal world.
 */
TEE_Result TA_InvokeCommandEntryPoint(void __maybe_unused *sess_ctx,
			uint32_t cmd_id,
			uint32_t param_types, TEE_Param params[4])
{
	(void)&sess_ctx; /* Unused parameter */

	switch (cmd_id) {
	case TA_FCE_QUERY_STATE:
	case TA_FCE_QUERY_ISR_CNT:
	case TA_FCE_QUERY_RESULT_CNT:
		return fce_query_result_cnt(sess_ctx, param_types, params);
	case TA_FCE_SET_RESULT_CNT:
		return fce_set_result_cnt(sess_ctx, param_types, params);
	case TA_FCE_SET_ATTR:
		return fce_set_attr(sess_ctx, param_types, params);
	case TA_FCE_GET_ATTR:
		return fce_get_attr(sess_ctx, param_types, params);
	case TA_FCE_START:
		return fce_do_compare(sess_ctx, param_types, params);
	case TA_FCE_STOP:
		return fce_stop(sess_ctx, param_types, params);
	case TA_FCE_GET_RESULT:
		return fce_get_result(sess_ctx, param_types, params);
	case TA_FEATURE_LIB_CREATE:
		return fce_create_featurelib(sess_ctx, param_types, params);
	case TA_FEATURE_LIB_FREE:
		return fce_release_featurelib(sess_ctx, param_types, params);
	case TA_FEATURE_LIB_SET:
		return fce_set_featurelib(sess_ctx, param_types, params);
	default:
		return TEE_ERROR_BAD_PARAMETERS;
	}
}
