/* SPDX-License-Identifier: BSD-2-Clause */
/*
 * Copyright (c) 2023, Alibaba Group Holding Limited
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

/* OP-TEE TEE client API (built by optee_client) */
#include <tee_client_api.h>

/* For the UUID (found in the TA's h-file(s)) */
#include <fce_ta.h>
#include <fce_cfg.h>

#define FCEDEV_NAME "/dev/light-fce"

#define FEATURE_COUNT 1001

static int *lib_buf2;

/* Reference result used to checking the result output */
static struct top_result expect_top1[] = {
	{0x19c0f, 0x3e9},
	{0x8c4d, 0x203},
	{0x74b8, 0x366},
	{0x6f2b, 0xd6},
	{0x65fa, 0x3e8},
	{0x5e54, 0x3a7},
	{0x5aeb, 0x389},
	{0x55c0, 0x29c},
};

static int target_vect[TARGET_VECTOR_CNT] = {
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
	0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
};

static int prepro_vect[PREPRO_VECTOR_CNT] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};

#define _MAXLEN 512
char a_line[_MAXLEN];
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

struct top_result top_buf[TOPREG_CNT]; // output vector

#if 0
static const unsigned int fceBaseNum[BASENUM_MAX+1] = {1, 10, 10000, 50000,
					100000, 200000, 500000, 1000000};
static const unsigned int fceSigsel[SIGSEL_MAX+1] = {0, 1};
static const unsigned int fceEndiansel[FCE_ENDIAN_MAX+1] = {0, 1};
#endif
static const unsigned int fceDim[DIM_MAX+1] = {128, 160, 192, 224, 256};
static const unsigned int fceWidth[INT_WIDTH_MAX+1] = {8, 4};

/* 
 * Funtion used to read featurelib from file
 */
static int load_featurelib_fromfile(char *buf, const char *filename)
{
	FILE *fp;
	int len;
	int data;
	int n = 0;

	fp = fopen(filename, "r");
	if (fp == NULL)
		return -1;

	while (fgets(a_line, _MAXLEN, fp) != NULL) {
		len = strlen(a_line);
		a_line[len-1] = '\0';  // remove enter character
		data = atoi(a_line);
		//printf("%d\n", data);
		buf[n++] = (char)data;
	}

	fclose(fp);

	return 0;
}

/* 
 * Funtion used to read vetor from file
 */
static int fill_target_vector_fromfile(char *buf, const char *filename)
{
	FILE *fp;
	int n = 0;

	fp = fopen(filename, "r");
	if (fp == NULL)
		return -1;

	while (fgets(a_line, _MAXLEN, fp) != NULL) {
		const float scale = 512.0f;
		float tmp, data;
		int len;
		signed char feat;

		len = strlen(a_line);
		a_line[len-1] = '\0';  // remove enter character
		data = (float)atof(a_line);
		tmp = data * scale;
		feat = max(min(tmp, 127), -127);
		buf[n++] = feat;
		//printf("%s %f %d\n", a_line, data, feat);
	}

	fclose(fp);

	return 0;
}

/* 
 * Funtion used to check the result 
 */
static TEEC_Result fce_check_result(struct top_result *res1, struct top_result *res2)
{
	TEEC_Result res;

	// compare top1 result
	if (res1->match_index == res2->match_index
		&& res1->match_value == res2->match_value) {
		printf("fce top result check ok,  test pass\n");
		res = TEEC_SUCCESS;
	} else {
		printf("fce top result check FAIL!!!\n");
		res = TEEC_ERROR_GENERIC;
	}

	return res;
}

static int fce_config_init(struct fce_cfg *cfg)
{
	cfg->base_num = FEATURE_COUNT; // feature library count 100w
	cfg->offset_base = 0; // search from here in library, should 4K align
	cfg->dim = DIM_256; // dimension 256
	cfg->width = INT_WIDTH_8; // INT_8
	cfg->sigsel = SIGN; // unsigned
	cfg->prepro_endian = FCE_LIT_ENDIAN; // xor vector endian
	cfg->target_endian = FCE_LIT_ENDIAN; // target vector endian
	cfg->base_endian = FCE_LIT_ENDIAN; // feature library endian

	cfg->readnum = fceDim[cfg->dim] * fceWidth[cfg->width] / BITS_PER_BYTE;
	if (cfg->readnum > sizeof(struct target_vector) * TARGET_VECTOR_CNT ||
		cfg->readnum > sizeof(struct prepro_vector) * PREPRO_VECTOR_CNT) {
		fprintf(stderr, "fce readnum<%d> is overflown\n", cfg->readnum);
		return -1;
	}

	return 0;
}

static TEEC_Result fce_featureCompare(TEEC_Session *sess, struct fce_cfg *cfg, int *target_vect,
						struct top_result *top_buf)
{
	unsigned int i, result_cnt;
	struct target_vector vect[TARGET_VECTOR_CNT];
	TEEC_Result res;
	TEEC_Operation op;
	uint32_t err_origin;

	// fill with feature vector from FCE input data
	memcpy(vect, target_vect, cfg->readnum);

	/************************* get result counter ************************/
	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	memset(&op, 0, sizeof(op));
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_OUTPUT, TEEC_NONE,
					 TEEC_NONE, TEEC_NONE);
	res = TEEC_InvokeCommand(sess, TA_FCE_QUERY_RESULT_CNT, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS) {
		printf("TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);
		goto exit;
	}
	result_cnt = op.params[0].value.a;

	/************************* set attribution ************************/
	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	memset(&op, 0, sizeof(op));
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_NONE,
					 TEEC_NONE, TEEC_NONE);
	
	op.params[0].tmpref.buffer = cfg;
	op.params[0].tmpref.size = sizeof(*cfg);
	res = TEEC_InvokeCommand(sess, TA_FCE_SET_ATTR, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS) {
		printf("TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);
		goto exit;
	}

	/************************* start fce ************************/
	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	memset(&op, 0, sizeof(op));
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_NONE,
					 TEEC_NONE, TEEC_NONE);
	op.params[0].tmpref.buffer = vect;
	op.params[0].tmpref.size = sizeof(vect);
	res = TEEC_InvokeCommand(sess, TA_FCE_START, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS) {
		printf("TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);
		goto exit;
	}

	/************************* get result ************************/
	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	memset(&op, 0, sizeof(op));
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_OUTPUT, TEEC_NONE,
					 TEEC_NONE, TEEC_NONE);
	op.params[0].tmpref.buffer = top_buf;
	op.params[0].tmpref.size = sizeof(struct top_result) * result_cnt;
	res = TEEC_InvokeCommand(sess, TA_FCE_GET_RESULT, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS) {
		printf("TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);
		goto exit;
	}

	/************************* check the result ************************/
	for (i = 0; i < result_cnt ; i++) {
		printf("FCE TOPREG<%d>:\tmatch_index=0x%x,\tmatch_value=0x%x\n",
			i, top_buf[i].match_index, top_buf[i].match_value);
		if (fce_check_result(top_buf, expect_top1) != TEEC_SUCCESS) {
			printf("@@E: FCE check result failed\r\n");
			goto exit;
		}
	}

	printf("@@: FCE check result SUCCESS\r\n");

exit:
	/************************* Stop fce ************************/
	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	memset(&op, 0, sizeof(op));
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_NONE, TEEC_NONE,
					 TEEC_NONE, TEEC_NONE);
	op.params[0].tmpref.buffer = top_buf;
	op.params[0].tmpref.size = sizeof(struct top_result) * result_cnt;
	res = TEEC_InvokeCommand(sess, TA_FCE_STOP, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS) {
		printf("TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);
		goto exit;
	}

	return res;
}


static TEEC_Result fce_demo_test(TEEC_Session *sess, struct fce_cfg *cfg)
{
	int ret = 0;

	printf("fce demo test\n");

	// fill xor pattern of feature library
	memcpy(cfg->prepro_vect, prepro_vect, cfg->readnum);

	ret = fce_featureCompare(sess, cfg, target_vect, top_buf);

	return ret;
}

static TEEC_Result create_featurelib(TEEC_Session *sess, unsigned int size,void **buf)
{
	TEEC_Result res;
	TEEC_Operation op;
	uint32_t err_origin;

	/* Clear the TEEC_Operation struct */
	memset(&op, 0, sizeof(op));

	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INPUT, TEEC_VALUE_OUTPUT,
					 TEEC_NONE, TEEC_NONE);
	op.params[0].value.a = size;

	/*
	 * TA_HELLO_WORLD_CMD_INC_VALUE is the actual function in the TA to be
	 * called.
	 */
	res = TEEC_InvokeCommand(sess, TA_FEATURE_LIB_CREATE, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS)
		printf("TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);
	else
		*buf = (void *)(((unsigned long)op.params[1].value.a << 32) | op.params[1].value.b);

	return res;
}


static TEEC_Result set_featurelib(TEEC_Session *sess, void *src, void *dst_buf,
									unsigned long offset, unsigned int size)
{
	TEEC_Result res;
	TEEC_Operation op;
	uint32_t err_origin;

	/* Clear the TEEC_Operation struct */
	memset(&op, 0, sizeof(op));

	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INPUT, TEEC_VALUE_INPUT,
					 TEEC_MEMREF_TEMP_INPUT, TEEC_NONE);
	op.params[0].value.a = (unsigned long)dst_buf >> 32;
	op.params[0].value.b = (unsigned long)dst_buf & 0xffffffff;
	
	op.params[1].value.a = (unsigned long)offset >> 32;
	op.params[1].value.b = (unsigned long)offset & 0xffffffff;
	op.params[2].tmpref.buffer = src;
	op.params[2].tmpref.size = size;

	/*
	 * TA_HELLO_WORLD_CMD_INC_VALUE is the actual function in the TA to be
	 * called.
	 */
	res = TEEC_InvokeCommand(sess, TA_FEATURE_LIB_SET, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS)
		printf("TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);

	return res;
}

static TEEC_Result release_featurelib(TEEC_Session *sess, void *buf)
{
	TEEC_Result res;
	TEEC_Operation op;
	uint32_t err_origin;

	/* Clear the TEEC_Operation struct */
	memset(&op, 0, sizeof(op));

	/*
	 * Prepare the argument. Pass a value in the first parameter,
	 * the remaining three parameters are unused.
	 */
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INPUT, TEEC_NONE,
					 TEEC_NONE, TEEC_NONE);
	op.params[0].value.a = (unsigned long)buf >> 32;
	op.params[0].value.b = (unsigned long)buf & 0xffffffff;

	/*
	 * TA_HELLO_WORLD_CMD_INC_VALUE is the actual function in the TA to be
	 * called.
	 */
	res = TEEC_InvokeCommand(sess, TA_FEATURE_LIB_FREE, &op,
				 &err_origin);
	if (res != TEEC_SUCCESS)
		errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x",
			res, err_origin);

	return res;
}

int main(int argc, const char **argv)
{
	struct fce_cfg cfg;
	unsigned int lib_size1;
	unsigned int lib_size2;
	unsigned int lib_size3;
	void *dst_buf1;
	void *dst_buf2;
	void *dst_buf3;
	TEEC_Result res;
	TEEC_Context ctx;
	TEEC_Session sess;
	TEEC_UUID uuid = TA_FCE_UUID;
	uint32_t err_origin;

	if (argc < 3) {
		printf("\nusage: ./fce_demo <target_vector.txt> <feature_library.txt>\n");
		return -1;
	}

	/* Initialize a context connecting us to the TEE */
	res = TEEC_InitializeContext(NULL, &ctx);
	if (res != TEEC_SUCCESS)
		printf("TEEC_InitializeContext failed with code 0x%x", res);

	/*
	 * Open a session to the "hello world" TA, the TA will print "hello
	 * world!" in the log when the session is created.
	 */
	res = TEEC_OpenSession(&ctx, &sess, &uuid,
			       TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
	if (res != TEEC_SUCCESS) {
		printf("TEEC_Opensession failed with code 0x%x origin 0x%x",
			res, err_origin);
		goto finalize_ctx;
	}

	/*
	 * Initialize the configuration
	 */
	fce_config_init(&cfg);

	/*
	 * Preparing feature library
	 */
	lib_size1 = 10*1024*1024;
	lib_size2 = cfg.base_num * cfg.readnum;
	lib_size3 = 1*1024*1024;

	res = create_featurelib(&sess, lib_size1, &dst_buf1);
	res |= create_featurelib(&sess, lib_size2, &dst_buf2);
	res |= create_featurelib(&sess, lib_size3, &dst_buf3);
	if (res != TEEC_SUCCESS) {
		printf("\nfailed to create featurelib\n");
		goto free_buf;
	}

	/* fill feature library from url argv[2]*/
	lib_buf2 = malloc(lib_size2);
	if (!lib_buf2) {
		printf("failed to malloc buffer[size: 0x%x]", lib_size2);
		goto free_buf;
	}

	if (load_featurelib_fromfile((char *)lib_buf2, argv[2]) < 0) {
		printf("\nfail to open library txt file\n");
		goto free_buf;
	}
	res = set_featurelib(&sess, lib_buf2, dst_buf2, 0, lib_size2);
	if (res != TEEC_SUCCESS) {
		printf("\nfailed to set featurelib\n");
		goto free_buf;
	}
	printf("preparing target vector\n");
	/* fill target vector from url argv[1]*/
	if (fill_target_vector_fromfile((char *)target_vect, argv[1]) < 0) {
		printf("\nfail to fill target vecotr\n");
		goto free_buf;
	}

	cfg.offset_base = (unsigned long)dst_buf2;
	fce_demo_test(&sess, &cfg);

free_buf:
	if (lib_buf2)
		free(lib_buf2);
	if (dst_buf1)
		release_featurelib(&sess, dst_buf1);
	if (dst_buf2)
		release_featurelib(&sess, dst_buf2);
	if (dst_buf3)
		release_featurelib(&sess, dst_buf3);

	TEEC_CloseSession(&sess);
finalize_ctx:
	TEEC_FinalizeContext(&ctx);
	printf("\nfce_demo: this is the end\n");

	return -1;
}


