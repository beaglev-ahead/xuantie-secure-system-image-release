// SPDX-License-Identifier: GPL-2.0
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <memory.h>
#include <malloc.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/poll.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <sys/socket.h>
#include <linux/netlink.h>

#define _MAXLEN 512
char a_line[_MAXLEN];

struct timeval t_begin, t_end;
struct timezone tz;

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

int print_timecost_us(char *string, struct timeval *t_begin, struct timeval *t_end)
{
	int us = (t_end->tv_sec - t_begin->tv_sec)*1000*1000
			+ (t_end->tv_usec - t_begin->tv_usec);

	printf("%s: <%d> us\n", string==NULL?__func__:string, us);
	return us;
}

int print_timecost_ms(struct timeval *t_begin, struct timeval *t_end)
{
	int ms = ((t_end->tv_sec - t_begin->tv_sec)*1000*1000
			+ (t_end->tv_usec - t_begin->tv_usec))/1000;

	printf("%s: <%d> ms\n", __func__, ms);
	return ms;
}


int fill_buffer(int *buf, int offset, int len, int data)
{
	int i;

	for (i = offset; i < offset+len; i += 4)
		buf[i/4] = data;

	return 0;
}

int fill_byte(char *buf, int offset, int len, char data)
{
	int i;

	for (i = offset; i < offset+len; i += 1)
		buf[i] = data;

	return 0;
}

int load_featurelib(char *buf, char *lib, int len)
{
	int i;

	for (i = 0; i < len; i++) {
		buf[i] = lib[i];
	}

	return 0;
}

int load_featurelib_fromfile(char *buf, const char *filename)
{
	FILE *fp;
	int len;
	int data;
	int n = 0;

	fp = fopen(filename, "r");
	if (fp == NULL)
		return -1;

	while (fgets(a_line, _MAXLEN, fp) != NULL) {
		len = strlen(a_line);
		a_line[len-1] = '\0';  // remove enter character
		data = atoi(a_line);
		//printf("%d\n", data);
		buf[n++] = (char)data;
	}

	fclose(fp);

	return 0;
}

int fill_target_vector(char *buf, char *vect, int len)
{
	int i;

	for (i = 0; i < len; i++) {
		buf[i] = vect[i];
	}

	return 0;
}

int fill_target_vector_fromfile(char *buf, const char *filename)
{
	FILE *fp, *fp_w;
	int n = 0;

	fp = fopen(filename, "r");
	if (fp == NULL)
		return -1;

	while (fgets(a_line, _MAXLEN, fp) != NULL) {
		const float scale = 512.0f;
		float tmp, data;
		int len;
		signed char feat;

		len = strlen(a_line);
		a_line[len-1] = '\0';  // remove enter character
		data = (float)atof(a_line);
		tmp = data * scale;
		feat = max(min(tmp, 127), -127);
		buf[n++] = feat;
		//printf("%s %f %d\n", a_line, data, feat);
	}

	fclose(fp);

	return 0;
}

