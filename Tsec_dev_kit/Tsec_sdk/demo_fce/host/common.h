/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _COMMON_H_
#define _COMMON_H_

#include <sys/socket.h>
#include <linux/netlink.h>

#if defined(__cplusplus)
extern "C" {
#endif

int print_timecost_us(char *string, struct timeval *t_begin, struct timeval *t_end);
int print_timecost_ms(struct timeval *t_begin, struct timeval *t_end);

int fill_buffer(int *buf, int offset, int len, int data);
int fill_byte(char *buf, int offset, int len, char data);
int load_featurelib(char *buf, char *lib, int len);
int load_featurelib_fromfile(char *buf, const char *filename);

int fill_target_vector(char *buf, char *vect, int len);
int fill_target_vector_fromfile(char *buf, const char *filename);

#if defined(__cplusplus)
}
#endif

#endif // #ifndef _COMMON_H_
